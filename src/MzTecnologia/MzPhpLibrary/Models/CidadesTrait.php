<?php

namespace MzTecnologia\MzPhpLibrary\Models;

use Illuminate\Support\Facades\Request;

/**
 * Trait que implementa a função de obter o array de cidades.
 * Esta função é chamada pela trait de ajax e também deve ser chamada na função de
 * create e edit. Quando é edit, deve ser chamada com o estadoId.
 * Funciona juntamente com o js cidade-estado.js
 * A rota a ser criada deve ser um get em /cidade/array/{estadoId} para ModeloController@ajaxCidadesArray
 */
trait CidadesTrait {
    
    public static function getCidadesArray($class, $estadoId = null)
    {
        //Verifica se há um estado preenchido no formulário, no caso de erro.
        //O old deve ter prioridade sobre o estado de entidade
        $oldEstadoId = Request::old('cidade.estado_id');
        
        $query = $class::query();

        if ($oldEstadoId !== null) {
            $query->where('estado_id', $oldEstadoId);
        }
        else if ($estadoId !== null) {
            $query->where('estado_id', $estadoId);
        } else {
            $query->where('estado_id', 1);
        }
        
        $array = $query->orderBy('nome', 'asc')
                       ->lists('nome', 'id');
        $array->prepend('', '');
        return $array;
    }
}
