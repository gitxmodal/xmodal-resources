<?php

namespace MzTecnologia\MzPhpLibrary\Models;

use Illuminate\Http\Request;

/**
 * Trait que implementa o ajax para 
 */
trait AjaxCidadesTrait {
    
    public function ajaxCidadesArray(Request $request, $estadoId)
    {
        $model = $this->getModel();
        if ($estadoId) {
            $array = $model::getCidadesArray($model, $estadoId);
        } else {
            $array = [];
        }
        return json_encode($array);
    }
}
