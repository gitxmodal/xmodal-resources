<div class="col-md-12">
    <label class="col-md-{{$labelSize}} control-label">{{$label}}</label>
    <div class="col-md-{{$contentSize}}">
        @if ($link)
            <a href="{{$link}}">
                <p>{{$value or '-'}}</p>
            </a>
        @else
            <p>{{$value or '-'}}</p>
        @endif
    </div>
</div>