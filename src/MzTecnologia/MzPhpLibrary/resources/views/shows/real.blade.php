<div class="col-md-12">
    <label class="col-md-{{$labelSize}} control-label">{{$label}}</label>
    <div class="col-md-{{$contentSize}}">
        <p>{{ Helper::decimalToReal($value) }}</p>
    </div>
</div>