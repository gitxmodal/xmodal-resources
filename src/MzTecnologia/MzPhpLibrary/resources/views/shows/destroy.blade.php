<form action="{{$action}}" method="POST" class="form-table js-delete">
    {{ csrf_field() }}
    {{ method_field('DELETE') }}
    <button type="submit" class="btn btn-sm btn-danger">
        {{$label}}
    </button>
</form>