<div class="col-md-12">
    <label class="col-md-{{$labelSize}} control-label">{{$label}}</label>
</div>
<div class="col-md-12">
    <div class="col-md-{{$contentSize}}">
        @if($field)
            <a href="{{$url}}" target="_blank">
                <img src="{{$url}}" class="img-responsive">
            </a>
        @else
            <p>{{$notFoundMsg}}</p>
        @endif
    </div>
</div>