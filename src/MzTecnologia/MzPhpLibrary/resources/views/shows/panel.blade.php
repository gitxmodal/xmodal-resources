<div class="panel-body">
    <div class="panel-title m-b-lg">
        <h3>
            {{$label}}
        </h3>
    </div>
    @include($view, [$name => $value])
</div>