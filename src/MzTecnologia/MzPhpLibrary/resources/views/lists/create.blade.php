<a href="{{ $action }}">
    <button class="btn btn-sm btn-primary">{{ $label }}</button>
</a>