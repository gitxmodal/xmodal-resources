{!! Form::open(array('action' => $action, 'method' => 'GET', 'class' => 'inline m-r-sm m-l-sm')) !!}
    <div class="inline">
        {!! Form::text('busca', old('busca', $busca), array("class" => "form-control", 
                       'placeholder' => $placeholder, 
                       'title' => $placeholder)) !!}
    </div>
    <div class="inline">
        <button class="btn btn-sm btn-primary" type="submit">
            <i class="fa fa-search"></i>
        </button>
    </div>
{!! Form::close() !!}