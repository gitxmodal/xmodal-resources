<div class="form-table">
    <a href="#" class="btn btn-xs btn-outline" data-toggle="modal" data-target="#{{$modalId}}">
        @if ($title)
            <i class="fa {{$faIcon}} icon {{$cssColor}} text-center" title="{{$title}}"></i>
        @else
            <i class="fa {{$faIcon}} icon {{$cssColor}} text-center"></i>
        @endif
    </a>
</div>