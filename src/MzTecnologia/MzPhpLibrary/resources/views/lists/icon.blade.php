<div class="form-table">
    <a href="{{ $action }}" class="btn btn-xs btn-outline">
        @if ($title)
            <i class="fa {{$faIcon}} icon {{$cssColor}} text-center" title="{{$title}}"></i>
        @else
            <i class="fa {{$faIcon}} icon {{$cssColor}} text-center"></i>
        @endif
    </a>
</div>