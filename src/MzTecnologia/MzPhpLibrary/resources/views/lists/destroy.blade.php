<form action="{{ $action }}" method="POST" class="form-table js-delete">
    {{ csrf_field() }}
    {{ method_field('DELETE') }}
    <button type="submit" class="btn btn-xs btn-outline">
        <i class="fa fa-trash-o icon text-danger text-center"></i>
    </button>
</form>