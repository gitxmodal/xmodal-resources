<label class="col-md-{{$labelSize}} control-label">{{$label}}</label>
<div class="col-md-{{$contentSize}} js-checkbox">
    {!! Form::checkbox($name, $old ? $old : old($name), $extra) !!}
</div>