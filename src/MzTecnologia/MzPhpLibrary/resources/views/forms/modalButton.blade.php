<button type="button" class="{{$extra['class']}}" data-toggle="modal" data-target="#{{$modalId}}">
    {{$label}}
</button>