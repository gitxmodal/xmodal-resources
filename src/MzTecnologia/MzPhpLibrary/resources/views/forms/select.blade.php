<label class="col-md-{{$labelSize}} control-label">{{$label}}</label>
<div class="col-md-{{$contentSize}}">
    {!! Form::select($name, $array, $old ? $old : old($name), $extra) !!}
</div>