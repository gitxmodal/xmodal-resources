<label class="col-md-{{$labelSize}} control-label">{{$label}}</label>
<div class="col-md-{{$contentSize}}">
    {!! Form::password($name, $extra) !!}
</div>