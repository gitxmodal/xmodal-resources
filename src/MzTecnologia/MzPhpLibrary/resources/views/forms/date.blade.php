<label class="col-md-{{$labelSize}} control-label">{{$label}}</label>
<div class="col-md-{{$contentSize}} js-date">
    <div class="input-group date">
        {!! Form::text($name, $old ? $old : old($name), $extra) !!}
        <span class="input-group-addon">
            <i class="fa fa-calendar"></i>
        </span>
    </div>
</div>