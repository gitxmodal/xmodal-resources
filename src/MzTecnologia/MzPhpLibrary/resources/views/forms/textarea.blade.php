<label class="col-md-{{$labelSize}} control-label">{{$label}}</label>
<div class="col-md-{{$contentSize}}">
    {!! Form::textarea($name, $old ? $old : old($name), $extra) !!}
</div>