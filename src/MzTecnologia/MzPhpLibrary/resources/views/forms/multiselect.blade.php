<label class="col-md-{{$labelSize}} control-label">{{$label}}</label>
<div class="col-md-{{$contentSize}}">
    @if ( $oldArray )
        {!! Form::select($name.'[]', $array, old($name.'[]', $oldArray ),
                        array('class' => 'chosen-select form-control', 
                              'multiple' => '', 'tabindex' => '4',
                              'data-placeholder' => $placeholder)) !!}
    @else
        {!! Form::select($name.'[]', $array, old($name.'[]'),
                        array('class' => 'chosen-select form-control', 
                              'multiple' => '', 'tabindex' => '4',
                              'data-placeholder' => $placeholder)) !!}
    @endif
</div>