<?php

namespace MzTecnologia\MzPhpLibrary\Forms;

use \View;
use Illuminate\Support\Facades\Request;

/**
 * Classe que implementa campos para listagem.
 */
class MZList {

    const VIEWS_PATH = "vendor/gitxmodal/xmodal-resources/src/MzTecnologia/MzPhpLibrary/resources/views/";
    
    static $formStarted = false;
    
    private static function startForms()
    {
        if (!self::$formStarted) {
            \View::addLocation(base_path(self::VIEWS_PATH));
            self::$formStarted = true;
        }
    }
    
    public static function iconsOpen()
    {
        self::startForms();
        
        return View::make('lists.iconsOpen');
    }
    
    public static function iconsClose()
    {
        self::startForms();
        
        return View::make('lists.iconsClose');
    }
    
    public static function create($label, $action)
    {
        self::startForms();
        
        return View::make('lists.create', [
            'label' => $label,
            'action' => $action
        ]);
    }
    
    /**
     * Cria o índice de paginação abaixo da tabela
     * @param type $list Lista utilizada no index
     * @return type A view
     */
    public static function paginate($list)
    {
        self::startForms();
        
        return View::make('lists.paginate', [
            'list' => $list,
        ]);
    }
    
    /**
     * Cria um campo de busca na seção de botões.
     * 
     * @param type $action A ação da rota de índice
     * @param type $placeholder A descrição de title e placeholder
     */
    public static function search($action, $placeholder = "")
    {
        self::startForms();
        
        $busca = Request::input('busca');
        
        return View::make('lists.search', [
            'action' => $action,
            'placeholder' => $placeholder,
            'busca' => $busca,
        ]);
    }
    
    /**
     * Ícone de edição
     */
    public static function edit($action)
    {
        return self::icon($action, 'fa-pencil-square-o', 'text-info');
    }
    
    public static function modalIcon($modalId, $faIcon, $cssColor = 'text-primary', $title = null)
    {
        self::startForms();
        
        return View::make('lists.modalIcon', [
            'modalId' => $modalId, 
            'faIcon' => $faIcon,
            'cssColor' => $cssColor,
            'title' => $title
        ]);
    }
    
    /**
     * Ícone genérico utilizando anchor <a>
     */
    public static function icon($action, $faIcon, $cssColor = 'text-primary', $title = null)
    {
        self::startForms();
        
        return View::make('lists.icon', [
            'action' => $action, 
            'faIcon' => $faIcon,
            'cssColor' => $cssColor,
            'title' => $title
        ]);
    }
    
    /**
     * Ícone de remoção
     */
    public static function destroy($action)
    {
        self::startForms();
        
        return View::make('lists.destroy', [
            'action' => $action
        ]);
    }
}