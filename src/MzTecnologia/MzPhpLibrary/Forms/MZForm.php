<?php

namespace MzTecnologia\MzPhpLibrary\Forms;

use \View;

/**
 * Classe que implementa Forms mais comuns do Blade.
 */
class MZForm {

    const VIEWS_PATH = "vendor/gitxmodal/xmodal-resources/src/MzTecnologia/MzPhpLibrary/resources/views/";
    
    static $formStarted = false;
    
    private static function startForms()
    {
        if (!self::$formStarted) {
            \View::addLocation(base_path(self::VIEWS_PATH));
            self::$formStarted = true;
        }
    }
    
    /**
     * Abre um formulário de post.
     */
    public static function open($variableName, $controllerStore = null, $class = null)
    {
        self::startForms();
        
        if (!$controllerStore) {
            $controllerStore = self::getControllerName($variableName, 'store');
        }
        
        return View::make('forms.open', [
            'storeArray' => self::getStoreArray($controllerStore, $class),
        ]);
    }

    /**
     * Abre um formulário de patch.
     */
    public static function model($variableName, $variable, $controllerUpdate = null, $class = null)
    {
        self::startForms();
        
        if (!$controllerUpdate) {
            $controllerUpdate = self::getControllerName($variableName, 'update');
        }
        
        return View::make('forms.model', [
            'variable' => $variable,
            'updateArray' => self::getUpdateArray($controllerUpdate, $class, $variable),
        ]);
    }

    /**
     * Abre um formulário de post com upload de arquivos
     */
    public static function openFile($variableName, $controllerStore = null, $class = null)
    {
        self::startForms();
        
        if (!$controllerStore) {
            $controllerStore = self::getControllerName($variableName, 'store');
        }
        
        return View::make('forms.open', [
            'storeArray' => self::getFileStoreArray($controllerStore, $class),
        ]);
    }
    
    /**
     * Abre um formulário de patch com upload de arquivos
     */
    public static function modelFile($variableName, $variable, $controllerUpdate = null, $class = null)
    {
        self::startForms();
        
        if (!$controllerUpdate) {
            $controllerUpdate = self::getControllerName($variableName, 'update');
        }
        
        return View::make('forms.model', [
            'variable' => $variable,
            'updateArray' => self::getFileUpdateArray($controllerUpdate, $class, $variable),
        ]);
    }
    
    private static function getStoreArray($controllerStore, $class)
    {
        $array = [
            'action' => $controllerStore,
            'class' => 'form-horizontal',
            'method' => 'POST'
        ];
        
        return self::filterExtra($array, $class);
    }
    
    private static function getUpdateArray($controllerUpdate, $class, $variable)
    {
        $array = [
            'action' => [$controllerUpdate, $variable],
            'class' => 'form-horizontal',
            'method' => 'PATCH'
        ];
        
        return self::filterExtra($array, $class);
    }
    
    private static function getFileStoreArray($controllerStore, $class)
    {
        $array = self::getStoreArray($controllerStore, $class);
        $array['files'] = true;
        return $array;
    }
    
    private static function getFileUpdateArray($controllerUpdate, $class, $variable)
    {
        $array = self::getUpdateArray($controllerUpdate, $class, $variable);
        $array['files'] = true;
        return $array;
    }
    
    private static function getControllerName($variableName, $function)
    {
        $nome = ucfirst($variableName);
        return $nome."Controller@$function";
    }
    
    /**
     * Fecha o Form
     */
    public static function close()
    {
        self::startForms();
        
        return View::make('forms.close');
    }
    
    /**
     * Abre uma seção para inputs.
     */
    public static function sectionOpen($size = 12, $class = null)
    {
        self::startForms();
        
        $extra['class'] = $class;
        
        return View::make('forms.sectionOpen', [
            'extra' => self::filterExtra($extra, 'form-group col-md-'.$size)
        ]);
    }
    
    /**
     * Fecha uma seção.
     */
    public static function sectionClose()
    {
        self::startForms();
        
        return View::make('forms.sectionClose');
    }
    
    /**
     * Input de texto
     */
    public static function text($label, $name, $old = null, $extra = null, $labelSize = 1, $contentSize = 5)
    {
        self::startForms();
        
        return View::make('forms.text', [
            'label' => $label,
            'name' => $name,
            'old' => $old,
            'labelSize' => $labelSize,
            'contentSize' => $contentSize,
            'extra' => self::filterExtra($extra, 'form-control')
        ]);
    }
    
    /**
     * Input para textarea
     */
    public static function textarea($label, $name, $old = null, $extra = null, $labelSize = 1, $contentSize = 5)
    {
        self::startForms();
        
        return View::make('forms.textarea', [
            'label' => $label,
            'name' => $name,
            'old' => $old,
            'labelSize' => $labelSize,
            'contentSize' => $contentSize,
            'extra' => self::filterAreaExtra($extra, 'form-control')
        ]);
    }
    
    /**
     * Input para password
     */
    public static function password($label, $name, $extra = null, $labelSize = 1, $contentSize = 5)
    {
        self::startForms();
        
        return View::make('forms.password', [
            'label' => $label,
            'name' => $name,
            'labelSize' => $labelSize,
            'contentSize' => $contentSize,
            'extra' => self::filterExtra($extra, 'form-control')
        ]);
    }
    
    /**
     * Input para select.
     */
    public static function select($label, $name, $array, $old = null, $extra = null, $labelSize = 1, $contentSize = 5)
    {
        self::startForms();
        
        return View::make('forms.select', [
            'label' => $label,
            'name' => $name,
            'array' => $array, 
            'old' => $old,
            'labelSize' => $labelSize,
            'contentSize' => $contentSize,
            'extra' => self::filterExtra($extra, 'form-control')
        ]);
    }
    
    /**
     * Input para componente multiselect.
     * Model é o componente origem dos dados.
     * Oldfunction deve ser uma função no componente origem que retorna um array com os dados antigos.
     */
    public static function multiselect($label, $name, $array, $modelName, $oldArray, 
                                       $placeholder = '', $labelSize = 1, $contentSize = 5)
    {
        self::startForms();
        
        return View::make('forms.multiselect', [
            'label' => $label,
            'name' => $name,
            'array' => $array, 
            'modelName' => $modelName,
            'oldArray' => $oldArray,
            'placeholder' => $placeholder,
            'labelSize' => $labelSize,
            'contentSize' => $contentSize
        ]);
    }
    
    /**
     * Input para autocomplete.
     */
    public static function autocomplete($label, $name, $url, $old = null, $extra = null, $labelSize = 1, $contentSize = 5)
    {
        self::startForms();
        
        $extra['url'] = $url;
        
        return View::make('forms.autocomplete', [
            'label' => $label,
            'name' => $name,
            'old' => $old,
            'labelSize' => $labelSize,
            'contentSize' => $contentSize,
            'extra' => self::filterExtra($extra, 'form-control js-autocomplete autocomplete-suggestions')
        ]);
    }
    
    /**
     * Input para dinheiro com formato de reais.
     */
    public static function money($label, $name, $old = null, $extra = null, $labelSize = 1, $contentSize = 5)
    {
        self::startForms();
        
        return View::make('forms.text', [
            'label' => $label,
            'name' => $name,
            'old' => $old,
            'labelSize' => $labelSize,
            'contentSize' => $contentSize,
            'extra' => self::filterExtra($extra, 'form-control js-money')
        ]);
    }
    
    /**
     * Input para dinheiro com formato de doláres.
     */
    public static function moneyUs($label, $name, $old = null, $extra = null, $labelSize = 1, $contentSize = 5)
    {
        self::startForms();
        
        return View::make('forms.text', [
            'label' => $label,
            'name' => $name,
            'old' => $old,
            'labelSize' => $labelSize,
            'contentSize' => $contentSize,
            'extra' => self::filterExtra($extra, 'form-control js-money-us')
        ]);
    }
    
    /**
     * Input para dinheiro sem símbolo de moeda.
     */
    public static function moneyGeneric($label, $name, $old = null, $extra = null, $labelSize = 1, $contentSize = 5)
    {
        self::startForms();
        
        return View::make('forms.text', [
            'label' => $label,
            'name' => $name,
            'old' => $old,
            'labelSize' => $labelSize,
            'contentSize' => $contentSize,
            'extra' => self::filterExtra($extra, 'form-control js-money-generic')
        ]);
    }
    
    /**
     * Input para decimal com formato de porcentagem
     */
    public static function porc($label, $name, $old = null, $extra = null, $labelSize = 1, $contentSize = 5)
    {
        self::startForms();
        
        return View::make('forms.text', [
            'label' => $label,
            'name' => $name,
            'old' => $old,
            'labelSize' => $labelSize,
            'contentSize' => $contentSize,
            'extra' => self::filterExtra($extra, 'form-control js-porc')
        ]);
    }
    
    /**
     * Input de data com formatação e datepicker para dd/mm/yyyy
     */
    public static function date($label, $name, $old = null, $extra = null, $labelSize = 1, $contentSize = 5)
    {
        self::startForms();
        
        return View::make('forms.date', [
            'label' => $label,
            'name' => $name,
            'old' => $old,
            'labelSize' => $labelSize,
            'contentSize' => $contentSize,
            'extra' => self::filterExtra($extra, 'form-control')
        ]);
    }
    
    /**
     * Input de CNPJ com formatação 99.999.999/9999-99
     */
    public static function cnpj($label, $name, $old = null, $extra = null, $labelSize = 1, $contentSize = 5)
    {
        self::startForms();
        
        return View::make('forms.text', [
            'label' => $label,
            'name' => $name,
            'old' => $old,
            'labelSize' => $labelSize,
            'contentSize' => $contentSize,
            'extra' => self::filterExtra($extra, 'form-control js-cnpj')
        ]);
    }
    
    /**
     * Input de checkbox preparado para i-check
     */
    public static function checkbox($label, $name, $old = null, $extra = null, $labelSize = 1, $contentSize = 5)
    {
        self::startForms();
        
        return View::make('forms.checkbox', [
            'label' => $label,
            'name' => $name,
            'old' => $old,
            'labelSize' => $labelSize,
            'contentSize' => $contentSize,
            'extra' => self::filterExtra($extra, 'form-control')
        ]);
    }
    
    /**
     * Input para upload de arquivos
     */
    public static function file($label, $name, $extra = null, $labelSize = 1, $contentSize = 5)
    {
        self::startForms();
        
        return View::make('forms.file', [
            'label' => $label,
            'name' => $name,
            'labelSize' => $labelSize,
            'contentSize' => $contentSize,
            'extra' => self::filterExtra($extra, 'form-control')
        ]);
    }
    
    /**
     * Imprime um label de título para separar seções no form.
     */
    public static function title($label)
    {
        self::startForms();
        
        return View::make('forms.title', [
            'label' => $label
        ]);
    }
    
    /**
     * Botão de envio de formulário
     */
    public static function submit($label = "Salvar", $extra = null)
    {
        self::startForms();
        
        return View::make('forms.submit', [
            'label' => $label,
            'extra' => self::filterExtra($extra, 'btn btn-sm btn-primary pull-right m-t-n-xs')
        ]);
    }
    
    public static function modalOpen($modalId, $title = null)
    {
        self::startForms();
        
        return View::make('forms.modalOpen', [
            'modalId' => $modalId,
            'title' => $title,
        ]);
    }
    
    public static function modalFooter()
    {
        self::startForms();
        
        return View::make('forms.modalFooter');
    }
    
    
    public static function modalClose()
    {
        self::startForms();
        
        return View::make('forms.modalClose');
    }
    
    public static function modalButton($label, $modalId, $cssColor = 'btn-primary', $extra = null)
    {
        self::startForms();
        
        $clazz = "btn btn-sm $cssColor";
        
        return View::make('forms.modalButton', [
            'label' => $label,
            'modalId' => $modalId,
            'extra' => self::filterExtra($extra, $clazz)
        ]);
    }
    
    
    /**
     * Botão de voltar
     */
    public static function back($action = null, $label = "Voltar")
    {
        self::startForms();
        
        if (!$action) {
            $action = "javascript:history.back()";
        }
        
        return View::make('forms.back', [
            'label' => $label,
            'action' => $action
        ]);
    }
    
    private static function filterExtra($extra, $class) 
    {
        if ($extra && key_exists('class', $extra)) {
            $str = $extra['class'];
            $extra['class'] = $class . ' ' . $str;
        } else {
            $extra['class'] = $class;
        }
        return $extra;
    }
    
    private static function filterAreaExtra($extra, $class) 
    {
        if ($extra && key_exists('class', $extra)) {
            $str = $extra['class'];
            $extra['class'] = $class . ' ' . $str;
        } else {
            $extra['class'] = $class;
        }
        
        if ($extra && !key_exists('rows', $extra)) {
            $extra['rows'] = 2;
        }
        
        return $extra;
    }
}