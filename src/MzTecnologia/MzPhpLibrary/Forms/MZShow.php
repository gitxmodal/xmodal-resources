<?php

namespace MzTecnologia\MzPhpLibrary\Forms;

use \View;

/**
 * Classe que implementa campos de Show.
 */
class MZShow {

    const VIEWS_PATH = "vendor/gitxmodal/xmodal-resources/src/MzTecnologia/MzPhpLibrary/resources/views/";
    
    static $formStarted = false;
    
    private static function startForms()
    {
        if (!self::$formStarted) {
            \View::addLocation(base_path(self::VIEWS_PATH));
            self::$formStarted = true;
        }
    }
    
    /**
     * Base para um panel que imprime partials
     */
    public static function panel($label, $view, $name, $value)
    {
        self::startForms();
        
        return View::make('shows.panel', [
            'label' => $label,
            'view' => $view,
            'name' => $name,
            'value' => $value
        ]);
    }
    
    /**
     * Imprime o título de uma seção no show
     */
    public static function title($label)
    {
        self::startForms();
        
        return View::make('shows.title', [
            'label' => $label,
        ]);
    }
    
    /**
     * Botão de criar
     */
    public static function create($action, $label = "Criar")
    {
        self::startForms();
        
        return View::make('shows.create', [
            'label' => $label,
            'action' => $action
        ]);
    }
    
    /**
     * Botão de editar
     */
    public static function edit($action, $label = "Editar")
    {
        self::startForms();
        
        return View::make('shows.edit', [
            'label' => $label,
            'action' => $action
        ]);
    }
    
    /**
     * Botão de remover
     */
    public static function destroy($action, $label = "Remover")
    {
        self::startForms();
        
        return View::make('shows.destroy', [
            'label' => $label,
            'action' => $action
        ]);
    }
    
    /**
     * Imprime um campo como plain text
     */
    public static function text($label, $value, $link = null, $labelSize = 2, $contentSize = 10)
    {
        self::startForms();
        
        return View::make('shows.text', [
            'label' => $label,
            'value' => $value,
            'link' => $link,
            'labelSize' => $labelSize,
            'contentSize' => $contentSize
        ]);
    }
    
    /**
     * Imprime um campo como date. Converte uma string de Y-m-d para d/m/Y
     */
    public static function date($label, $value, $labelSize = 2, $contentSize = 10)
    {
        self::startForms();
        
        return View::make('shows.date', [
            'label' => $label,
            'value' => $value,
            'labelSize' => $labelSize,
            'contentSize' => $contentSize
        ]);
    }
    
    /**
     * Imprime um campo como BR Real. Converte de decimal para R$ xx.xxx,xx
     */
    public static function real($label, $value, $labelSize = 2, $contentSize = 10)
    {
        self::startForms();
        
        return View::make('shows.real', [
            'label' => $label,
            'value' => $value,
            'labelSize' => $labelSize,
            'contentSize' => $contentSize
        ]);
    }
    
    /**
     * Imprime um campo como US Dolár. Converte de decimal para U$ xx,xxx.xx
     */
    public static function dolar($label, $value, $labelSize = 2, $contentSize = 10)
    {
        self::startForms();
        
        return View::make('shows.dolar', [
            'label' => $label,
            'value' => $value,
            'labelSize' => $labelSize,
            'contentSize' => $contentSize
        ]);
    }
    
    /**
     * Imprime um campo como porcentagem. Converte de decimal para xxx,xx%
     */
    public static function porc($label, $value, $labelSize = 2, $contentSize = 10)
    {
        self::startForms();
        
        return View::make('shows.porc', [
            'label' => $label,
            'value' => $value,
            'labelSize' => $labelSize,
            'contentSize' => $contentSize
        ]);
    }
    
    /**
     * Imprime um campo como CNPJ. Converte de string para 99.999.999/9999-99
     */
    public static function cnpj($label, $value, $labelSize = 2, $contentSize = 10)
    {
        self::startForms();
        
        return View::make('shows.cnpj', [
            'label' => $label,
            'value' => $value,
            'labelSize' => $labelSize,
            'contentSize' => $contentSize
        ]);
    }
    
    /**
     * Imprime uma imagem cadastrada. Deve ser passado o próprio campo e a url da imagem.
     */
    public static function image($label, $field, $url, $notFoundMsg = null, 
                                 $labelSize = 2, $contentSize = 6)
    {
        self::startForms();
        
        if (!$notFoundMsg) {
            $notFoundMsg = "Sem imagem cadastrada.";
        }
        
        return View::make('shows.image', [
            'label' => $label,
            'field' => $field,
            'url' => $url,
            'notFoundMsg' => $notFoundMsg,
            'labelSize' => $labelSize,
            'contentSize' => $contentSize
        ]);
    }
    
    /**
     * Imprime um campo apenas com label e vazio
     */
    public static function void($label = "-")
    {
        self::startForms();
        
        return View::make('shows.void', [
            'label' => $label
        ]);
    }
}