<?php

namespace MzTecnologia\MzPhpLibrary\Files;

use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Classe que guarda um arquivo que veio de upload
 */
class MZFile {
    
    const ERROR_NO_ERROR = 0;
    const ERROR_WRITING_FILE = 1;
    const ERROR_WITH_UPLOADED_FILE = 2;
    
    const DEFAULT_PATH = "uploads/outros";
    
    private $uploadedFile;
    private $originalExtension = "";
    private $originalFilename = "";
    private $newFilename = "";
    private $path = "";
    
    private $error;
    
    public function __construct(UploadedFile $uploadedFile, $path = null) {
        if ($uploadedFile) {
            $this->uploadedFile = $uploadedFile;
            $this->originalFilename = $uploadedFile->getClientOriginalName();
            $this->originalExtension = $uploadedFile->getClientOriginalExtension();

            if ($path !== null) {
                $this->path = $path;
            } else {
                $this->path = self::DEFAULT_PATH;
            }
            
            $this->error = self::ERROR_NO_ERROR;
        } else {
            $this->error = self::ERROR_WITH_UPLOADED_FILE;
        }
    }

    public function salvaArquivo()
    {
        $i = 0;
        do {
            $fileName = rand(111111111,999999999).'.'.$this->originalExtension;
            if (file_exists($this->getDestinationPath().'/'.$fileName)) {
                $file = null;
            } else {
                $file = $this->uploadedFile->move($this->getDestinationPath(), $fileName);
            }
            $i++;
        } while ($file === null && $i < 5);
        
        if ($file === null) {
            $this->error = self::ERROR_WRITING_FILE;
            return null;
        } else {
            $this->newFilename = $fileName;
            return $fileName;
        }
    }
    
    public function copy($path, $filename)
    {
        return copy($this->getFilePath(true), $path . "/" . $filename);
    }

    public function getUploadedFile()
    {
        return $this->uploadedFile;
    }
    
    public function getOriginalFilename()
    {
        return $this->originalFilename;
    }
    
    public function getOriginalExtension()
    {
        return $this->originalExtension;
    }
    
    public function getNewFilename()
    {
        return $this->newFilename;
    }
    
    public function getFilePath($withFilename = true)
    {
        if ($withFilename) {
            return $this->path . "/" . $this->newFilename;
        } else {
            return $this->path;
        }
    }
    
    public function delete()
    {
        return unlink($this->getFilePath(true));
    }
    
    public function getError()
    {
        return $this->error;
    }
    
    private function getDestinationPath()
    {
        return public_path().'/'.$this->path;
    }
}