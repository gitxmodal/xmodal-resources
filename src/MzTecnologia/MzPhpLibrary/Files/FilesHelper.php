<?php

namespace MzTecnologia\MzPhpLibrary\Files;

/**
 * Classe de Helper para Arquivos PHP
 */
class FilesHelper {
    
    /**
     * Apaga um arquivo do local especificado. Apaga do public.
     * 
     * @param type $filename Filename with path. Ex: uploads/files
     */
    public static function deleteFile($filename)
    {
        $completePath = self::getCompletePath($filename);
        
        //Fecha o arquivo
        fclose($completePath);
        
        //Apaga
        unlink($completePath);
    }
    
    /**
     * Completa um arquivo com o endereço da pasta pública.
     * 
     * @param type $filename Ex: uploads/files/file.ext
     * @return type Ex: /var/www/project/public/uploads/files/file.ext
     */
    public static function getCompletePath($filename)
    {
        return public_path().'/'.$filename;
    }
}