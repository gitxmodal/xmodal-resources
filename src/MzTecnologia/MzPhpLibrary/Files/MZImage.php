<?php

namespace MzTecnologia\MzPhpLibrary\Files;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Imagick;

/**
 * Classe que guarda uma imagem que foi alvo de upload
 */
class MZImage {
    
    const ERROR_NO_ERROR = 0;
    const ERROR_WRITING_FILE = 1;
    const ERROR_WITH_UPLOADED_FILE = 2;
    
    const DEFAULT_PATH = "uploads/outros";
    const TMP_PATH = "uploads/tmp";
    
    private $uploadedFile;
    private $originalExtension = "";
    private $originalFilename = "";
    private $newFilename = "";
    private $path = "";
    
    private $newSizeW;
    private $newSizeH;
    private $cropX;
    private $cropY;
    private $cropW;
    private $cropH;
    
    private $error;
    
    public function __construct(UploadedFile $uploadedFile, $path = null, 
                                $cropX = null, $cropY = null, $cropW = null, 
                                $cropH = null, $sizeW = null, $sizeH = null) {
        if ($uploadedFile) {
            $this->uploadedFile = $uploadedFile;
            $this->originalFilename = $uploadedFile->getFilename();
            $this->originalExtension = $uploadedFile->getClientOriginalExtension();
            
            if ($path !== null) {
                $this->path = $path;
            } else {
                $this->path = self::DEFAULT_PATH;
            }
            $this->cropX = $cropX;
            $this->cropY = $cropY;
            $this->cropW = $cropW;
            $this->cropH = $cropH;
            $this->newSizeW = $sizeW;
            $this->newSizeH = $sizeH;
            
            $this->error = self::ERROR_NO_ERROR;
            
            $this->convert();
        } else {
            $this->error = self::ERROR_WITH_UPLOADED_FILE;
        }
    }
    
    public function getImageFilename()
    {
        return $this->newFilename;
    }
    
    public function getImagePath($withFilename = true)
    {
        if ($withFilename) {
            return $this->path . "/" . $this->newFilename;
        } else {
            return $this->path;
        }
    }
    
    public function getError()
    {
        return $this->error;
    }
    
    public function delete()
    {
        return unlink($this->getImagePath(true));
    }
    
    private function convert()
    {
        $filename = $this->salvaImagem();
        
        if ($filename === null) {
            $this->error = self::ERROR_WRITING_FILE;
        }
        
        $tmpFilenamePath = $this->getTempPath().'/'.$filename;
        $this->newFilename = $filename;
        
        $im = new Imagick();
        $im->readimage($tmpFilenamePath);
        
        if ($this->needsCrop()) {
            $im->cropimage($this->cropW, $this->cropH, $this->cropX, $this->cropY);
        }
        
        if ($this->needsResize()) {
            $im->thumbnailimage($this->newSizeW, $this->newSizeH, true);
        }
        
        $r = $im->writeImage ($this->getDestinationPath().'/'.$filename);
        
        if (file_exists($tmpFilenamePath)) {
            unlink($tmpFilenamePath);
        }
    }
    
    private function needsCrop()
    {
        return $this->cropW !== null && $this->cropH !== null && 
               $this->cropX !== null && $this->cropY !== null;
    }
    
    private function needsResize()
    {
        return $this->newSizeH !== null && $this->newSizeW !== null;
    }
    
    private function salvaImagem()
    {   
        $i = 0;
        do {
            $fileName = rand(111111111,999999999).'.'.$this->originalExtension;
            if (file_exists($this->getDestinationPath().'/'.$fileName)) {
                $file = null;
            } else {
                $file = $this->uploadedFile->move($this->getTempPath(), $fileName);
            }
            $i++;
        } while ($file === null && $i < 5);
        
        if ($file === null) {
            $this->error = self::ERROR_WRITING_FILE;
            return null;
        } else {
            return $fileName;
        }
    }
    
    private function getDestinationPath()
    {
        return public_path().'/'.$this->path;
    }
    
    private function getTempPath()
    {
        return public_path().'/'.self::TMP_PATH;
    }
}