<?php

namespace MzTecnologia\MzPhpLibrary\Helpers;

use Carbon\Carbon;

class Param
{
    public static function filterStr($input)
    {
        if ($input === null) {
            return null;
        }
        
        $end = trim($input);
        if ($end !== "") {
            return $end;
        }
        
        return null;
    }
    
    public static function filterData($input)
    {
        if ($input === null) {
            return null;
        }
        
        $end = trim($input);
        if ($end !== "") {
            return Param::dateBrToIso($end);
        }
        
        return null;
    }
    
    public static function dateBrToIso($data)
    {
        return self::convertDate('d/m/Y', 'Y-m-d', $data);
        
    }
    
    public static function datetimeBrToIso($data)
    {
        return self::convertDate('d/m/Y H:i:s', 'Y-m-d H:i:s', $data);
        
    }
    
    private static function convertDate($inputFormat, $outputFormat, $input)
    {
        if ($input === null || $input === "") {
            return null;
        } else {
            $carbonDate = Carbon::createFromFormat($inputFormat, $input);
            return $carbonDate->format($outputFormat);
        }
    }
}
