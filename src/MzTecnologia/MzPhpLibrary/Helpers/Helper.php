<?php

namespace MzTecnologia\MzPhpLibrary\Helpers;

use Auth;

class Helper
{
   
    public static function getCurrentUserNome($ret = "-") {
        $user = Auth::user();
        if ($user) {
            return $user->nome;
        } else {
            return $ret;
        }
    }
    
    public static function getUserNomeById($id) {
        $user = User::find($id);
        
        $nome = '-';
        if ($user instanceof User) {
            $nome = $user->nome;
        }
        
        return $nome;
    }
    
    public static function successMessage($msg)
    {
        \Session::flash('message', $msg); 
    }
    
    public static function errorMessage($msg)
    {
        \Session::flash('errormessage', $msg); 
    }
    
    public static function decimalToReal($valor, $nullReturn = '-')
    {
        if ($valor == null) {
            return $nullReturn;
        } else {
            if ($valor < 0) {
                return '- R$ '.number_format(($valor*-1), 2, ',', '.');
            } else {
                return 'R$ '.number_format($valor, 2, ',', '.');
            }
        }
    }
    
    public static function decimalToDolar($valor, $nullReturn = '-')
    {
        if ($valor == null) {
            return $nullReturn;
        } else {
            if ($valor < 0) {
                return '- U$ '.number_format(($valor*-1), 2, '.', ',');
            } else {
                return 'U$ '.number_format($valor, 2, '.', ',');
            }
        }
    }
    
    public static function decimalToMoneyGeneric($valor, $nullReturn = '-')
    {
        if ($valor == null) {
            return $nullReturn;
        } else {
            if ($valor < 0) {
                return number_format(($valor*-1), 2, ',', '.');
            } else {
                return number_format($valor, 2, ',', '.');
            }
        }
    }
    
    public static function decimalToPorc($valor, $nullReturn = '-')
    {
        if ($valor == null) {
            return $nullReturn;
        } else {
            if ($valor < 0) {
                return '- '.number_format(($valor*-1), 2, ',', '.').'%';
            } else {
                return number_format($valor, 2, ',', '.').'%';
            }
        }
    }
    
    public static function realToDecimal($real, $nullReturn = '-')
    {
        if (!$real) {
            return $nullReturn;
        }
        $real = trim(str_replace('R$', '', $real));
        $real = str_replace('.', '', $real);
        $real = str_replace(',', '.', $real);
        return floatval($real);
    }
    
    public static function moneyGenericToDecimal($money, $nullReturn = '-')
    {
        if (!$money) {
            return $nullReturn;
        }
        $money = str_replace('.', '', $money);
        $money = str_replace(',', '.', $money);
        return floatval($money);
    }
    
    public static function dolarToDecimal($dolar, $nullReturn = '-')
    {
        if (!$dolar) {
            return $nullReturn;
        }
        $dolar = trim(str_replace('U$', '', $dolar));
        $dolar = str_replace(',', '', $dolar);
        return floatval($dolar);
    }
    
    public static function porcToDecimal($porc, $nullReturn = '-')
    {
        if (!$porc) {
            return $nullReturn;
        }
        $porc = trim(str_replace('%', '', $porc));
        $porc = str_replace('.', '', $porc);
        $porc = str_replace(',', '.', $porc);
        return floatval($porc);
    }
    
    public static function dateIsoToBr($data, $nullReturn = '-')
    {
        if ($data == null) {
            return $nullReturn;
        } else {
            return date('d/m/Y',strtotime($data));
        }
    }
    
    public static function dateIsoToUs($data, $nullReturn = '-')
    {
        if ($data == null) {
            return $nullReturn;
        } else {
            return date('m/d/Y',strtotime($data));
        }
    }
    
    public static function dateBrToIso($data, $nullReturn = '-')
    {
        if ($data != null) {
            $dateTime = \DateTime::createFromFormat('d/m/Y', $data);
            if ($dateTime) {
                return $dateTime->format('Y-m-d');
            }
        }
        return $nullReturn;
    }
    
    public static function dateTimeIsoToBr($data, $nullReturn = '-')
    {
        if ($data == null) {
            return $nullReturn;
        } else {
            return date('d/m/Y H:i',strtotime($data));
        }
    }
    
    public static function dateTimeIsoToUs($data, $nullReturn = '-')
    {
        if ($data == null) {
            return $nullReturn;
        } else {
            return date('m/d/Y H:i',strtotime($data));
        }
    }
    
    public static function booleanToStr($bool, $firstCapital = true)
    {
        if ($bool == true) {
            if ($firstCapital) {
                return "Sim";
            } else {
                return "sim";
            }
        } 
        else if ($bool == false) {
            if ($firstCapital) {
                return "Não";
            } else {
                return "não";
            }
        }
        else {
            return "-";
        }
    }
    
    /**
     * Faz um resumo de texto (string).
     * 
     * @param type $text String de texto.
     * @param type $maxSize Tamanho máximo do resultado. (Min 3)
     * @return type Retorna o resumo.
     */
    public static function subText($text, $maxSize)
    {
        if ($text == null) {
            return '-';
        }
        else if ($maxSize > 3) {
            if (strlen($text) > $maxSize) {
                return substr($text, 0, $maxSize - 3) . '...';
            }
        }
        return $text;
    }
    
    public static function formatCNPJ($valor)
    {
        $valor = str_replace('.', '', $valor);
        $valor = str_replace('/', '', $valor);
        $valor = str_replace('-', '', $valor);
        
        return $valor;
    }
    
    /**
     * Formata CNPJ para exibição na view a partir de
     * um integer
     * 
     * @param type $nbr_cnpj ex: 12345678000123 -> 12.345.678/0001-23
     */
    public static function integerToCnpj($nbrCnpj)
    {
        if (strlen($nbrCnpj) !== 14) {
            return $nbrCnpj;
        }
        
        $p1 = substr($nbrCnpj, 0, 2);
        $p2 = substr($nbrCnpj, 2, 3);
        $p3 = substr($nbrCnpj, 5, 3);
        $p4 = substr($nbrCnpj, 8, 4);
        $p5 = substr($nbrCnpj, 12, 2);

        return "$p1.$p2.$p3/$p4-$p5";
    }
    
    /**
     * Função que pega o valor antigo para
     * forms nested.
     * 
     * @param type $key O atributo requerido
     * @param type $entity A entidade objeto do model.
     * @return string
     */
    public static function oldNested($key, $entity)
    {
        if (!is_null($entity))
        {
            if (isset($entity[$key]))
            {
                return $entity[$key];
            }
        }
        else
        {
            return '';
        }
    }
    
    /**
     * Função para old de multiselect
     * 
     * @param type $entity O vetor no formato lists('nome', 'id)
     * @return string O proprio vetor se existente
     */
    public static function oldMultiSelect($entity)
    {
        if (!is_null($entity))
        {
            return $entity;
        }
        else
        {
            return [];
        }
    }
    
    public static function getStatusArray()
    {
        return [
            0 => 'Ativo',
            1 => 'Inativo',
        ];
    }
    
    public static function getMesesArray()
    {
        return [
            1 => 'Janeiro',
            2 => 'Fevereiro',
            3 => 'Março',
            4 => 'Abril',
            5 => 'Maio', 
            6 => 'Junho', 
            7 => 'Julho',
            8 => 'Agosto',
            9 => 'Setembro',
            10 => 'Outubro',
            11 => 'Novembro',
            12 => 'Dezembro'
        ];
    }
    
    public static function getEstadosArray()
    {              
        return [
            'AC' => 'Acre',
            'AL' => 'Alagoas',
            'AP' => 'Amapá',
            'AM' => 'Amazonas',
            'BA' => 'Bahia',
            'CE' => 'Ceará',
            'DF' => 'Distrito Federal',
            'ES' => 'Espirito Santo',
            'GO' => 'Goiás',
            'MA' => 'Maranhão',
            'MS' => 'Mato Grosso do Sul',
            'MT' => 'Mato Grosso',
            'MG' => 'Minas Gerais',
            'PA' => 'Pará',
            'PB' => 'Paraíba',
            'PR' => 'Paraná',
            'PE' => 'Pernambuco',
            'PI' => 'Piauí',
            'RJ' => 'Rio de Janeiro',
            'RN' => 'Rio Grande do Norte',
            'RS' => 'Rio Grande do Sul',
            'RO' => 'Rondônia',
            'RR' => 'Roraima',
            'SC' => 'Santa Catarina',
            'SP' => 'São Paulo',
            'SE' => 'Sergipe',
            'TO' => 'Tocantins',
        ];
    }
    
    
    public static function getPaisesArray()
    {              
        return [
            'Brasil' => 'Brasil',
            'United States' => 'United States',
            'United Kingdom' => 'United Kingdom',
            'Afghanistan' => 'Afghanistan',
            'Aland Islands' => 'Aland Islands',
            'Albania' => 'Albania',
            'Algeria' => 'Algeria',
            'American Samoa' => 'American Samoa',
            'Andorra' => 'Andorra',
            'Angola' => 'Angola',
            'Anguilla' => 'Anguilla',
            'Antarctica' => 'Antarctica',
            'Antigua and Barbuda' => 'Antigua and Barbuda',
            'Argentina' => 'Argentina',
            'Armenia' => 'Armenia',
            'Aruba' => 'Aruba',
            'Australia' => 'Australia',
            'Austria' => 'Austria',
            'Azerbaijan' => 'Azerbaijan',
            'Bahamas' => 'Bahamas',
            'Bahrain' => 'Bahrain',
            'Bangladesh' => 'Bangladesh',
            'Barbados' => 'Barbados',
            'Belarus' => 'Belarus',
            'Belgium' => 'Belgium',
            'Belize' => 'Belize',
            'Benin' => 'Benin',
            'Bermuda' => 'Bermuda',
            'Bhutan' => 'Bhutan',
            'Bolivia, Plurinational State of' => 'Bolivia, Plurinational State of',
            'Bonaire, Sint Eustatius and Saba' => 'Bonaire, Sint Eustatius and Saba',
            'Bosnia and Herzegovina' => 'Bosnia and Herzegovina',
            'Botswana' => 'Botswana',
            'Bouvet Island' => 'Bouvet Island',
            'British Indian Ocean Territory' => 'British Indian Ocean Territory',
            'Brunei Darussalam' => 'Brunei Darussalam',
            'Bulgaria' => 'Bulgaria',
            'Burkina Faso' => 'Burkina Faso',
            'Burundi' => 'Burundi',
            'Cambodia' => 'Cambodia',
            'Cameroon' => 'Cameroon',
            'Canada' => 'Canada',
            'Cape Verde' => 'Cape Verde',
            'Cayman Islands' => 'Cayman Islands',
            'Central African Republic' => 'Central African Republic',
            'Chad' => 'Chad',
            'Chile' => 'Chile',
            'China' => 'China',
            'Christmas Island' => 'Christmas Island',
            'Cocos (Keeling) Islands' => 'Cocos (Keeling) Islands',
            'Colombia' => 'Colombia',
            'Comoros' => 'Comoros',
            'Congo' => 'Congo',
            'Congo, The Democratic Republic of The' => 'Congo, The Democratic Republic of The',
            'Cook Islands' => 'Cook Islands',
            'Costa Rica' => 'Costa Rica',
            "Cote D'ivoire" => "Cote D'ivoire",
            'Croatia' => 'Croatia',
            'Cuba' => 'Cuba',
            'Curacao' => 'Curacao',
            'Cyprus' => 'Cyprus',
            'Czech Republic' => 'Czech Republic',
            'Denmark' => 'Denmark',
            'Djibouti' => 'Djibouti',
            'Dominica' => 'Dominica',
            'Dominican Republic' => 'Dominican Republic',
            'Ecuador' => 'Ecuador',
            'Egypt' => 'Egypt',
            'El Salvador' => 'El Salvador',
            'Equatorial Guinea' => 'Equatorial Guinea',
            'Eritrea' => 'Eritrea',
            'Estonia' => 'Estonia',
            'Ethiopia' => 'Ethiopia',
            'Falkland Islands (Malvinas)' => 'Falkland Islands (Malvinas)',
            'Faroe Islands' => 'Faroe Islands',
            'Fiji' => 'Fiji',
            'Finland' => 'Finland',
            'France' => 'France',
            'French Guiana' => 'French Guiana',
            'French Polynesia' => 'French Polynesia',
            'French Southern Territories' => 'French Southern Territories',
            'Gabon' => 'Gabon',
            'Gambia' => 'Gambia',
            'Georgia' => 'Georgia',
            'Germany' => 'Germany',
            'Ghana' => 'Ghana',
            'Gibraltar' => 'Gibraltar',
            'Greece' => 'Greece',
            'Greenland' => 'Greenland',
            'Grenada' => 'Grenada',
            'Guadeloupe' => 'Guadeloupe',
            'Guam' => 'Guam',
            'Guatemala' => 'Guatemala',
            'Guernsey' => 'Guernsey',
            'Guinea' => 'Guinea',
            'Guinea-bissau' => 'Guinea-bissau',
            'Guyana' => 'Guyana',
            'Haiti' => 'Haiti',
            'Heard Island and Mcdonald Islands' => 'Heard Island and Mcdonald Islands',
            'Holy See (Vatican City State)' => 'Holy See (Vatican City State)',
            'Honduras' => 'Honduras',
            'Hong Kong' => 'Hong Kong',
            'Hungary' => 'Hungary',
            'Iceland' => 'Iceland',
            'India' => 'India',
            'Indonesia' => 'Indonesia',
            'Iran, Islamic Republic of' => 'Iran, Islamic Republic of',
            'Iraq' => 'Iraq',
            'Ireland' => 'Ireland',
            'Isle of Man' => 'Isle of Man',
            'Israel' => 'Israel',
            'Italy' => 'Italy',
            'Jamaica' => 'Jamaica',
            'Japan' => 'Japan',
            'Jersey' => 'Jersey',
            'Jordan' => 'Jordan',
            'Kazakhstan' => 'Kazakhstan',
            'Kenya' => 'Kenya',
            'Kiribati' => 'Kiribati',
            "Korea, Democratic People's Republic of" => "Korea, Democratic People's Republic of",
            'Korea, Republic of' => 'Korea, Republic of',
            'Kuwait' => 'Kuwait',
            'Kyrgyzstan' => 'Kyrgyzstan',
            "Lao People's Democratic Republic" => "Lao People's Democratic Republic",
            'Latvia' => 'Latvia',
            'Lebanon' => 'Lebanon',
            'Lesotho' => 'Lesotho',
            'Liberia' => 'Liberia',
            'Libya' => 'Libya',
            'Liechtenstein' => 'Liechtenstein',
            'Lithuania' => 'Lithuania',
            'Luxembourg' => 'Luxembourg',
            'Macao' => 'Macao',
            'Macedonia, The Former Yugoslav Republic of' => 'Macedonia, The Former Yugoslav Republic of',
            'Madagascar' => 'Madagascar',
            'Malawi' => 'Malawi',
            'Malaysia' => 'Malaysia',
            'Maldives' => 'Maldives',
            'Mali' => 'Mali',
            'Malta' => 'Malta',
            'Marshall Islands' => 'Marshall Islands',
            'Martinique' => 'Martinique',
            'Mauritania' => 'Mauritania',
            'Mauritius' => 'Mauritius',
            'Mayotte' => 'Mayotte',
            'Mexico' => 'Mexico',
            'Micronesia, Federated States of' => 'Micronesia, Federated States of',
            'Moldova, Republic of' => 'Moldova, Republic of',
            'Monaco' => 'Monaco',
            'Mongolia' => 'Mongolia',
            'Montenegro' => 'Montenegro',
            'Montserrat' => 'Montserrat',
            'Morocco' => 'Morocco',
            'Mozambique' => 'Mozambique',
            'Myanmar' => 'Myanmar',
            'Namibia' => 'Namibia',
            'Nauru' => 'Nauru',
            'Nepal' => 'Nepal',
            'Netherlands' => 'Netherlands',
            'New Caledonia' => 'New Caledonia',
            'New Zealand' => 'New Zealand',
            'Nicaragua' => 'Nicaragua',
            'Niger' => 'Niger',
            'Nigeria' => 'Nigeria',
            'Niue' => 'Niue',
            'Norfolk Island' => 'Norfolk Island',
            'Northern Mariana Islands' => 'Northern Mariana Islands',
            'Norway' => 'Norway',
            'Oman' => 'Oman',
            'Pakistan' => 'Pakistan',
            'Palau' => 'Palau',
            'Palestinian Territory, Occupied' => 'Palestinian Territory, Occupied',
            'Panama' => 'Panama',
            'Papua New Guinea' => 'Papua New Guinea',
            'Paraguay' => 'Paraguay',
            'Peru' => 'Peru',
            'Philippines' => 'Philippines',
            'Pitcairn' => 'Pitcairn',
            'Poland' => 'Poland',
            'Portugal' => 'Portugal',
            'Puerto Rico' => 'Puerto Rico',
            'Qatar' => 'Qatar',
            'Reunion' => 'Reunion',
            'Romania' => 'Romania',
            'Russian Federation' => 'Russian Federation',
            'Rwanda' => 'Rwanda',
            'Saint Barthelemy' => 'Saint Barthelemy',
            'Saint Helena, Ascension and Tristan da Cunha' => 'Saint Helena, Ascension and Tristan da Cunha',
            'Saint Kitts and Nevis' => 'Saint Kitts and Nevis',
            'Saint Lucia' => 'Saint Lucia',
            'Saint Martin (French part)' => 'Saint Martin (French part)',
            'Saint Pierre and Miquelon' => 'Saint Pierre and Miquelon',
            'Saint Vincent and The Grenadines' => 'Saint Vincent and The Grenadines',
            'Samoa' => 'Samoa',
            'San Marino' => 'San Marino',
            'Sao Tome and Principe' => 'Sao Tome and Principe',
            'Saudi Arabia' => 'Saudi Arabia',
            'Senegal' => 'Senegal',
            'Serbia' => 'Serbia',
            'Seychelles' => 'Seychelles',
            'Sierra Leone' => 'Sierra Leone',
            'Singapore' => 'Singapore',
            'Sint Maarten (Dutch part)' => 'Sint Maarten (Dutch part)',
            'Slovakia' => 'Slovakia',
            'Slovenia' => 'Slovenia',
            'Solomon Islands' => 'Solomon Islands',
            'Somalia' => 'Somalia',
            'South Africa' => 'South Africa',
            'South Georgia and The South Sandwich Islands' => 'South Georgia and The South Sandwich Islands',
            'South Sudan' => 'South Sudan',
            'Spain' => 'Spain',
            'Sri Lanka' => 'Sri Lanka',
            'Sudan' => 'Sudan',
            'Suriname' => 'Suriname',
            'Svalbard and Jan Mayen' => 'Svalbard and Jan Mayen',
            'Swaziland' => 'Swaziland',
            'Sweden' => 'Sweden',
            'Switzerland' => 'Switzerland',
            'Syrian Arab Republic' => 'Syrian Arab Republic',
            'Taiwan, Province of China' => 'Taiwan, Province of China',
            'Tajikistan' => 'Tajikistan',
            'Tanzania, United Republic of' => 'Tanzania, United Republic of',
            'Thailand' => 'Thailand',
            'Timor-leste' => 'Timor-leste',
            'Togo' => 'Togo',
            'Tokelau' => 'Tokelau',
            'Tonga' => 'Tonga',
            'Trinidad and Tobago' => 'Trinidad and Tobago',
            'Tunisia' => 'Tunisia',
            'Turkey' => 'Turkey',
            'Turkmenistan' => 'Turkmenistan',
            'Turks and Caicos Islands' => 'Turks and Caicos Islands',
            'Tuvalu' => 'Tuvalu',
            'Uganda' => 'Uganda',
            'Ukraine' => 'Ukraine',
            'United Arab Emirates' => 'United Arab Emirates',
            'United Kingdom' => 'United Kingdom',
            'United States' => 'United States',
            'United States Minor Outlying Islands' => 'United States Minor Outlying Islands',
            'Uruguay' => 'Uruguay',
            'Uzbekistan' => 'Uzbekistan',
            'Vanuatu' => 'Vanuatu',
            'Venezuela, Bolivarian Republic of' => 'Venezuela, Bolivarian Republic of',
            'Viet Nam' => 'Viet Nam',
            'Virgin Islands, British' => 'Virgin Islands, British',
            'Virgin Islands, U.S.' => 'Virgin Islands, U.S.',
            'Wallis and Futuna' => 'Wallis and Futuna',
            'Western Sahara' => 'Western Sahara',
            'Yemen' => 'Yemen',
            'Zambia' => 'Zambia',
            'Zimbabwe' => 'Zimbabwe',           
             ];
    }

}
